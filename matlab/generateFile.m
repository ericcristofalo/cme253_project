%--------------------------------------------------------------------------
%
% File Name:      generateFile.m
% Date Created:   2017/02/11
% Date Modified:  2017/02/11
%
% Author:         Eric Cristofalo
% Contact:        eric.cristofalo@gmail.com
%
% Description:    CME 253 - Generate Image Matrix Files
%
% Inputs:         
%
% Outputs:        
%
%--------------------------------------------------------------------------

clean('Figures',0);

%% Generate Pyramid Images and Header Files

% Read Image
filePath = '/Users/ericcristofalo/git/cme253_project/Images/image_01/';
imRaw = imread([filePath,'im.png']);
imSize = size(imRaw);

% Make Square Image (From Horizontally Rectangular Image)
x = (imSize(2)-imSize(1))/2;
imSquare = imRaw(:,x+1:end-x,:);
imSize = size(imSquare);

% Number of Images to Produce
powerRange = 5:12;
powerRange = 10;
for p = powerRange
   if imSize(1)>=(2^p)
      disp(['Current Index: ',num2str(p)]);
      
      % Resize and Convert Images
      imCur = cv.resize(imSquare,[2^p,2^p]);
      imCurG = cv.cvtColor(imCur,'RGB2GRAY');
      sizeName = [num2str(2^p),'_',num2str(2^p)];
      % Write Images to File
%       imwrite(imCur,[filePath,'im_',sizeName,'.png']);
      imwrite(imCurG,[filePath,'im_g_',num2str(2^p),'_',num2str(2^p),'.png']);
      % Write Image Data to File
      fileID = fopen([filePath,'im_g_',sizeName,'.h'],'w');
%       fprintf(fileID,['const int img[',num2str(2^p),'][',num2str(2^p),'] = {\n']);
      fprintf(fileID,['const int img[',num2str(2^p*2^p),'] = {']);
      for i = 1:(2^p)
%          fprintf(fileID,'{');
         for j = 1:(2^p)
            val = imCurG(i,j);
            fprintf(fileID,'%3d',val);
%             if j~=(2^p)
               fprintf(fileID,',');
%             end
         end
%          fprintf(fileID,'},\n');
      end
      fprintf(fileID,'};');
      fclose(fileID);
   end
end
disp('Finished!');

