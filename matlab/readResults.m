%--------------------------------------------------------------------------
%
% File Name:      readResults.m
% Date Created:   2017/02/16
% Date Modified:  2017/02/17
%
% Author:         Eric Cristofalo
% Contact:        eric.cristofalo@gmail.com
%
% Description:    CME 253 - Reading and displaying results
%
% Inputs:         
%
% Outputs:        
%
%--------------------------------------------------------------------------

clean('Figures',0);

%% Read Result Image

figure(1); clf(1);

% User Input
imageFolder = 'Image_03';
filePath = ['/Users/ericcristofalo/git/cme253_project/Images/',imageFolder,'/im_g_1024_1024.png'];
fileOutPath = ['/Users/ericcristofalo/git/cme253_project/Images/',imageFolder,'/'];

% Read Original Image
imRaw = imread(filePath);
subplot(1,3,1); imshow(imRaw); impixelinfo;

% Set Output File Path
filePath = '/Users/ericcristofalo/Desktop/';
% filePath = '/Users/ericcristofalo/git/cme253_project/cuda_eric/';

% Read Output CPU Image
imCPU = dlmread([filePath,'result_cpu.txt'],',');
% subplot(1,3,2); imagesc(imCPU); axis equal; impixelinfo;
imRGB = scale2RGB(double(uint8(imCPU)+1));
subplot(1,3,2); subimage(imRGB); impixelinfo;
imwrite(imRGB,[fileOutPath,'result_cpu.png']);

% Read Output GPU Image
imGPU = dlmread([filePath,'result_gpu.txt'],',');
% subplot(1,3,3); imagesc(imGPU); axis equal; impixelinfo;
imRGB = scale2RGB(double(uint8(imGPU)+1));
subplot(1,3,3); subimage(imRGB); impixelinfo;
imwrite(imRGB,[fileOutPath,'result_gpu.png']);










