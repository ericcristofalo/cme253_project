# README #

CME 253 Final Project

2017/02/17

* Eric Cristofalo
* Zijian Wang

### Project Overview ###
* Implemented SuperPixel Clustering Algorithm (SLIC) in Parallel using CUDA on AWS

### Running the Code ###
Please first clone our public repository using git:

https://bitbucket.org/ericcristofalo/cme253_project

This will automatically clone the required images (stored as header files) that the code requires to run. There are three different images that may be tested with. 

Two GPU implementations are stored under cuda_implementation_1 and cuda_implementation_2 that correspond to Eric's and Zijian's implementations, respectively. Simply make the code in each directory and run the resulting executable: .superpixel

cuda_implementation_1 contains the CPU comparison. 

MATLAB scripts are also provided for generating test images and post-processing the results.