/*
 *  Copyright 2016 NVIDIA Corporation
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include <iostream>
#include <fstream>
#include <stdio.h>
//#include "cublas_v2.h"
#include <vector>
#include <math.h>

#include "debug.h"
#include "../Images/image_03/im_g_1024_1024.h" // include input image from file

using namespace std;

#define SIZE 1024 // require a square image as input
#define THREADS_PER_BLOCK_X 32
#define THREADS_PER_BLOCK_Y 32
#define K 64 // number of superpixels
#define K_edge 8 // number of superpixels per edge
#define S 128 // we check S = SIZE/sqrt(K) neighborhood

__global__ void slic_gpu(const int size, double scale, const int *img, int *clusters, int *result, int *dist) {
  // __shared__ int clusters[K][3+1]; // K is the number of clusters

  // Assign Clusters Centers with One Thread
  if (threadIdx.x==0 && threadIdx.y==0) {
  	// Initialize Clusters Centers If No Cluster History In Memory
  	if (clusters[0]==0) {
  		// if (blockIdx.x==0 && blockIdx.y==0) {
	  	// 	printf("Cluster Initialize: %d\n",clusters[0]);
	  	// }
	    int xInd = 0;
	    int yInd = 0;
	    for(int k=0; k<K; k++) {
	      xInd = (k % K_edge);
	      clusters[k*3+0] = S/2 + size/K_edge*xInd;
	      clusters[k*3+1] = S/2 + size/K_edge*yInd;
	      clusters[k*3+2] = img[ clusters[k*3+1]*size + clusters[k*3+0] ];
	      // Increase yInd
	      if (xInd==(K_edge-1)) {
	        yInd++;
	      }
	      // printf("cluster: %d\n", k);
	      // printf("cluster x: %d\n", clusters[k*3+0]);
	      // printf("cluster y: %d\n", clusters[k*3+1]);
	      // printf("cluster g: %d\n", clusters[k*3+2]);
	    }
  	}
  }
  __syncthreads();

  // Extract Current Pixel Coordinates
  const int curx = blockIdx.x * THREADS_PER_BLOCK_X + threadIdx.x;
  const int cury = blockIdx.y * THREADS_PER_BLOCK_Y + threadIdx.y;
  const int curImgInd = cury*size + curx;

	// Compute Distance Metric and Find Best Cluster
  int curDist;
	for(int k=0; k<K; k++) {
    int x = clusters[k*3+0];
    int y = clusters[k*3+1];
    int g = clusters[k*3+2];
 		if ( (curx>(x-S)) && (curx<(x+S)) && (cury>(y-S)) && (cury<(y+S)) ) {
    	int curg = img[curImgInd];
      int ds = ((curx-x)*(curx-x)) + ((cury-y)*(cury-y));
      int dc = ((curg-g)*(curg-g));
      curDist = round(sqrt(double(ds*scale + dc)));
 			if (curDist<dist[curImgInd]) {
        result[curImgInd] = k;
        dist[curImgInd] = curDist;
        // printf("result: %d\n",result[curImgInd]);
    	}
 		}
 	}

}

void slic_cpu(const int size, double scale, const int numIterations, const int *img, int *resultImg) {
  
  // Assign Cluster Centers
  int clusters[K][3];
  int xInd = 0;
  int yInd = 0;
  for(int k=0; k<K; k++) {
    xInd = (k % K_edge);
    clusters[k][0] = S/2 + size/K_edge*xInd;
    clusters[k][1] = S/2 + size/K_edge*yInd;
    clusters[k][2] = img[ clusters[k][1] * size + clusters[k][0] ];
    // Increase yInd
    if (xInd==(K_edge-1)) {
      yInd++;
    }
    // cout << "cluster: " << k << endl;
    // cout << (k % K_edge) << endl;
    // cout << clusters[k][0] << endl;
    // cout << clusters[k][1] << endl;
    // cout << clusters[k][2] << endl;
  }
  
  // Main Loop
  std::vector<double> distImg (size*size,1000000.0);
  for (int loopInd=0; loopInd<numIterations; loopInd++) {
    // For Each Cluster
    for (int k=0; k<K; k++) {
      // cout << "cluster id: " << k << endl;
      int x, y, g;
      x = clusters[k][0];
      y = clusters[k][1];
      g = clusters[k][2];
      // Initialize 2S x 2S Window
      std::vector<int> yVals;
      std::vector<int> xVals;
      int newVal;
      for (int i=-S; i<S; i++) {
        newVal = x+i;
        if (newVal>=0 && newVal<size) {
          xVals.push_back(newVal);
        }
        newVal = y+i;
        if (newVal>=0 && newVal<size) {
          yVals.push_back(newVal);
        }
      }
      // cout << "size of xVals: " << xVals.size() << endl;
      // for (int i=0; i<xVals.size(); i++) {
      //    cout << "xVals: " << xVals[i] << endl;
      // }
      // for (int i=0; i<yVals.size(); i++) {
      //    cout << "yVals: " << yVals[i] << endl;
      // }

      // For each Pixel in 2S Window About Current Cluster Center
      for (int i=0; i<xVals.size(); i++) {
        for (int j=0; j<yVals.size(); j++) {
          // cout << "xInd: " << i << " and yInd: " << j << endl;
          int curx, cury, curg;
          curx = xVals[i];
          cury = yVals[j];
          // cout << "curx: " << curx << " and cury: " << cury << endl;
          int curImgInd = cury * size + curx;
          curg = img[ curImgInd ];
          // cout << "curImgInd: " << curImgInd << " and curg: " << curg << endl;
          double ds, dc, dist;
          // Compute Current Distance
          ds = std::pow((double(curx)-double(x)),2.0) + std::pow((double(cury)-double(y)),2.0);
          dc = std::pow((double(curg)-double(g)),2.0);
          dist = std::pow((dc + ds*scale),0.5);
          if (dist<distImg[curImgInd]) {
            resultImg[curImgInd] = k;
            distImg[curImgInd] = dist;
            // cout << "resultImg: " << resultImg[curImgInd] << " and distImg: " << distImg[curImgInd] << endl;
          }
        }
      }
    }

    // Move Cluster Centers to New Locations
    if (loopInd!=(numIterations-1)) {
      double curErr = 0;
      int clustersNew[K][3];
      for (int k=0; k<K; k++) {
        double avgX = 0.0;
        double avgY = 0.0;
        double avgG = 0.0;
        int count = 0;
        for (int i=0; i<size; i++) {
          for (int j=0; j<size; j++) {
            if (resultImg[j*size+i]==k) {
              count++;
              avgX = avgY + double(i);
              avgY = avgY + double(j);
              avgG = avgG + double(resultImg[j*size+i]);
            }
          }
        }
        avgX = avgX/double(count);
        avgY = avgY/double(count);
        avgG = avgG/double(count);
        // cout << "Averages: " << avgX << avgY << avgG << endl;
        // Assign New Cluster Information
        clustersNew[k][0] = round(avgX);
        clustersNew[k][1] = round(avgY);
        clustersNew[k][2] = round(avgG);
        // Count Error
        curErr = curErr + std::pow((double(clusters[k][0])-avgX),2.0) + std::pow((double(clusters[k][1])-avgY),2.0) + std::pow((double(clusters[k][2])-avgG),2.0);
        // Assign New Cluster Center
        clusters[k][0] = clustersNew[k][0];
        clusters[k][1] = clustersNew[k][1];
        clusters[k][2] = clustersNew[k][2];
      }
      printf("Current CPU residual error: %f\n", curErr);
    }
  }
}

int main( int argc, char *argv[] )
{

  // GPU Device Number and Name
  int dev;
  cudaDeviceProp deviceProp;
  checkCUDA( cudaGetDevice( &dev ) );
  checkCUDA( cudaGetDeviceProperties( &deviceProp, dev ) );
  printf("Using GPU %d: %s\n", dev, deviceProp.name );
  
  // Generic SLIC Variables for Both Methods
  int numIterations = 10;
  double m = 80.0;
  double scale = m*m/double(S)/double(S);

  // ---------- ---------- ---------- ---------- ---------- ----------
  // GPU IMPLEMENTATION

  // Assigning Variables
  const int size = SIZE;
  int *d_img;
  int *d_clusters;
  int *h_clusters;
  int *d_result;
  int *h_result;
  int *d_dist;
  int *h_dist;

  // Allocating Memory
  size_t numbytes = size * size * sizeof( int );
  size_t numbytes_clusters = K * 3 * sizeof( int );
  // size_t numbytes_uint8 = size * size * 256;

  h_clusters = (int *) malloc( numbytes_clusters );
  h_result = (int *) malloc( numbytes );
  h_dist = (int *) malloc( numbytes );
  if( h_clusters==NULL || h_result==NULL || h_dist==NULL )
  {
    fprintf(stderr,"Error in host malloc\n");
    return 911;
  }

  // Start GPU Timer
  cudaEvent_t start, stop;
  checkCUDA( cudaEventCreate( &start ) );
  checkCUDA( cudaEventCreate( &stop ) );
  checkCUDA( cudaEventRecord( start, 0 ) );

  // Assign Memory on GPU
  checkCUDA( cudaMalloc( (void **)&d_img, numbytes ) );
  checkCUDA( cudaMalloc( (void **)&d_clusters, numbytes_clusters ) ); 
  checkCUDA( cudaMalloc( (void **)&d_result, numbytes ) ); 
  checkCUDA( cudaMalloc( (void **)&d_dist, numbytes ) ); 

  // Copy Memory to GPU
  checkCUDA( cudaMemcpy( d_img, img, numbytes, cudaMemcpyHostToDevice ) );
  checkCUDA( cudaMemset( d_clusters, 0, numbytes_clusters ) );
  checkCUDA( cudaMemset( d_result, 0, numbytes ) );
  checkCUDA( cudaMemset( d_dist, 1000000, numbytes ) );

  // Set Kernel Size
  dim3 threads1( THREADS_PER_BLOCK_X, THREADS_PER_BLOCK_Y, 1 );
  dim3 blocks1( 32, 32, 1 );

  // Run Iterations of GPU SLIC
  for (int loopInd=0; loopInd<numIterations; loopInd++) {

  	// Launch Kernel
  	slic_gpu<<< blocks1, threads1 >>> ( size, scale, d_img, d_clusters, d_result, d_dist);

		// Copy Result to CPU Memory
	  checkCUDA( cudaMemcpy( h_result, d_result, numbytes, cudaMemcpyDeviceToHost ) );

  	// Move Cluster Centers to New Locations
    if (loopInd!=(numIterations-1)) {

    	// Copy Cluster to CPU Memory
    	checkCUDA( cudaMemcpy( h_clusters, d_clusters, numbytes_clusters, cudaMemcpyDeviceToHost ) );

    	// Compute Cluster Averages
    	double curErr = 0;
	    int clustersNew[K*3];
		  for (int k=0; k<K; k++) {
		    double avgX = 0.0;
		    double avgY = 0.0;
		    double avgG = 0.0;
		    int count = 0;
		    for (int i=0; i<size; i++) {
		      for (int j=0; j<size; j++) {
		        if (h_result[j*size+i]==k) {
		          count++;
		          avgX = avgY + double(i);
		          avgY = avgY + double(j);
		          avgG = avgG + double(h_result[j*size+i]);
		        }
		      }
		    }
		    avgX = avgX/double(count);
		    avgY = avgY/double(count);
		    avgG = avgG/double(count);
		    // cout << "Averages: " << avgX << avgY << avgG << endl;
		    // Assign New Cluster Information
		    clustersNew[k*3+0] = round(avgX);
		    clustersNew[k*3+1] = round(avgY);
		    clustersNew[k*3+2] = round(avgG);
		    // Count Error
		    curErr = curErr + std::pow((double(h_clusters[k*3+0])-avgX),2.0) + std::pow((double(h_clusters[k*3+1])-avgY),2.0) + std::pow((double(h_clusters[k*3+2])-avgG),2.0);
		    // Assign New Cluster Center
		    h_clusters[k*3+0] = clustersNew[k*3+0];
		    h_clusters[k*3+1] = clustersNew[k*3+1];
		    h_clusters[k*3+2] = clustersNew[k*3+2];
		  }
	   	// Error
  		printf("Current GPU residual error: %f\n", curErr);

  		// Copy New Cluster to GPU Memory
		  checkCUDA( cudaMemcpy( d_clusters, h_clusters, numbytes_clusters, cudaMemcpyHostToDevice ) );
    }
  }

  // End GPU Timer
  checkCUDA( cudaEventRecord( stop, 0 ) );
  checkCUDA( cudaEventSynchronize( stop ) );
  float elapsedTime;
  checkCUDA( cudaEventElapsedTime( &elapsedTime, start, stop ) );

  // Finished GPU Implementation
  // for (int i=0; i<size*size; i++){
  // 	cout << "h_result[" << i << "]: " << h_result[i] << endl;
  // }  
  printf("\nFinished GPU version");
  fprintf(stdout, "Total GPU SLIC time is %f sec\n", elapsedTime / 1000.0f );
  fprintf(stdout, "Performance is %f GFlop/s\n", 
    2.0 * (double) size * (double) size * (double) size / 
    ( (double) elapsedTime / 1000.0 ) * 1.e-9 );

  // ---------- ---------- ---------- ---------- ---------- ----------
  // CPU IMPLEMENTATION

  // Start CPU Timer
  cudaEvent_t start2, stop2;
  checkCUDA( cudaEventCreate( &start2 ) );
  checkCUDA( cudaEventCreate( &stop2 ) );
  checkCUDA( cudaEventRecord( start2, 0 ) );

  // Run CPU Version of SLIC
  int resultImg[size*size];
  slic_cpu(size, scale, numIterations, img, resultImg);

  // End CPU Timers
  checkCUDA( cudaEventRecord( stop2, 0 ) );
  checkCUDA( cudaEventSynchronize( stop2 ) );
  float elapsedTime2;
  checkCUDA( cudaEventElapsedTime( &elapsedTime2, start2, stop2 ) );

  printf("\nFinished CPU version");
  fprintf(stdout, "Total CPU SLIC time is %f sec\n", elapsedTime2 / 1000.0f );
  fprintf(stdout, "Performance is %f GFlop/s\n", 
    2.0 * (double) size * (double) size * (double) size / 
    ( (double) elapsedTime2 / 1000.0 ) * 1.e-9 );
  printf("\n");

  // ---------- ---------- ---------- ---------- ---------- ----------

  // Write GPU Result to File
  ofstream myfile;
  myfile.open ("result_gpu.txt");
  for(int i=0; i<size; i++) {
  	for(int j=0; j<size; j++) {
  		if(j != size-1)
  			myfile << h_result[i*size + j] << ",";
  		else
  			myfile << h_result[i*size + j];
  	}
  	myfile << "\n";
  }
  myfile.close();

  // Write CPU Result to File
  ofstream myfile2;
  myfile2.open ("result_cpu.txt");
  for(int i=0; i<size; i++) {
    for(int j=0; j<size; j++) {
      if(j != size-1)
        myfile2 << resultImg[i*size + j] << ",";
      else
        myfile2 << resultImg[i*size + j];
    }
    myfile2 << "\n";
  }
  myfile2.close();

  cout << "All finished!" << endl;

  return 0;

}

